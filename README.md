
# Client-Server Application for Message Communication

This program consists of a client and server implementation for a messaging application. The client allows users to create accounts, log in, send messages to other users, read messages from their inbox, and perform various administrative tasks. The server manages the communication between clients, stores user accounts and messages in a database, and handles the execution of client commands.




## Installation

To run the client-server application, follow these steps:

1. Clone the repository to your local machine.
2. Install the required dependencies by running pip install -r requirements.txt.
3. Run the server script using python server.py.
4. In a separate terminal or command prompt, run the client script using python client.py.
5. Follow the prompts in the client application to interact with the server and send/receive messages.


Note: Make sure you have a compatible version of Python installed (Python 3.7 or above).
    
## Usage

1. Launch the client application by running the client.py script.
2. The client will connect to the server at the specified host and port.
3. Upon successful connection, the server will send a welcome message to the client.
4. The client will prompt you to enter commands for various actions such as creating an account, logging in, sending messages, and performing administrative tasks.
5. Follow the instructions provided by the client application to interact with the server and perform the desired actions.
6. To stop the server and exit the client application, use the stop command.
## Examples

#### Creating an account

To create a new account, use the create command. Enter a username and a password when prompted. If the account creation is successful, you will receive a confirmation message.

```Type command: create

Enter your username: myusername
Password (type 'reset' to send a request for resetting your password): mypassword
```
#### Logging in 

To log in to an existing account, use the login command. Enter your username and password when prompted. If the login is successful, you will receive a confirmation message.

```Type command: login

Enter your username: myusername
Password (type 'reset' to send a request for resetting your password): mypassword
```

#### Sending a message

To send a message to another user, use the send command. Enter the recipient's username and the message content when prompted. If the message is sent successfully, you will receive a confirmation message.

```Type command: send

Enter recipient name: recipientusername
Enter a message: Hello, this is my message!
```

#### Reading Inbox Messages

To read your messages from the inbox, use the read command. The client will display all the messages in your inbox, including the sender and the message content.

```Type command: read

[Message 1]
TIME: 2023-05-25 15:30:00
SENDER: senderusername
MESSAGE: Hello, this is a message from senderusername.

[Message 2]
TIME: 2023-05-25 16:45:00
SENDER: sender2username
MESSAGE: This is another message from sender2username.
```

#### Performing Administrative Tasks

If you have administrative privileges, you can perform certain tasks using the admin command. You can choose from options such as resetting a user's password, sending a message to all users, reading another user's inbox, or deleting a user's account.

```Type command: admin```

Type one of the following commands:
- reset (reset user password)
- sendall (send message to all users)
- readfor (read user inbox)
- delete (delete user account)




## Contributing

Contributions are welcome! If you find any issues or have suggestions for improvements, please create a new issue or submit a pull request.

