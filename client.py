import socket
import pwinput
import os
from dotenv import load_dotenv
from server_utils.communication import send_json, recv_json

class Client:
    def __init__(self):
        load_dotenv()
        self.host = os.getenv("HOST")
        self.port = int(os.getenv("PORT"))
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def connect(self):
        try:
            self.socket.connect((self.host, self.port))
        except ConnectionRefusedError:
            print("Failed to connect to the server. Please check the server status and try again.")
            exit(1)
        except Exception as e:
            print(f"An error occurred while connecting to the server: {str(e)}")
            exit(1)

    def receive_response(self):
        try:
            server_response = recv_json(self.socket)
            self.print_server_response(server_response)
        except TimeoutError:
            print("Server response timed out. Please try again.")
        except Exception as e:
            print(f"An error occurred while receiving the server response: {str(e)}")

    def print_server_response(self, response):
        print()
        for key, value in response.items():
            print(f"{key}\n{value}")

    def get_command(self):
        print()
        command = input("Type command: ")
        command = {"command": command}
        print()
        return command

    def get_credentials(self, command):
        username = input("Enter your username: ")
        password = pwinput.pwinput(prompt="Password (type 'reset' to send a request for reset your password): ", mask="*")
        return {"command": command, "username": username, "password": password}

    def send_message_command(self):
        recipient = input("Enter recipient name: ")
        message = input("Enter a message: ")
        return {"command": "send", "recipient": recipient, "message": message}

    def get_username(self):
        username = input("Enter username for whom you want to perform the operation: ")
        return username

    def get_admin_command(self):
        admin_command = input("Type one of the following commands: \
            \n- reset (reset user password) \
            \n- sendall (send message to all users) \
            \n- readfor (read user's inbox) \
            \n- delete (delete user's account)\n")
        return admin_command

    def send_to_all_command(self):
        message = input("Enter a message to send to all: ")
        return message

    def main(self):
        self.connect()
        welcome_message = recv_json(self.socket)
        self.print_server_response(welcome_message)

        while True:
            command = self.get_command()

            if command["command"] in ["create", "login"]:
                command = self.get_credentials(command["command"])
            elif command["command"] == "send":
                command = self.send_message_command()
            elif command["command"] == "admin":
                admin_command = self.get_admin_command()
                if admin_command in ["reset", "readfor", "delete"]:
                    username = self.get_username()
                    command = {"command": "admin", "admin_command": admin_command, "username": username}
                elif admin_command == "sendall":
                    message = self.send_to_all_command()
                    command = {"command": "admin", "admin_command": admin_command, "message": message}
                else:
                    print("Wrong command, try again.")
                    continue

            send_json(self.socket, command)

            if command["command"] == "stop":
                print("[STOP] Server has been stopped")
                break

            self.receive_response()

if __name__ == "__main__":
    client = Client()
    client.main()