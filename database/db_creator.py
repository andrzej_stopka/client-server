import logging
import os
import psycopg2
from dotenv import load_dotenv
from psycopg2.extensions import AsIs as asis

############################################

# the whole script executes after creating the class object and using the "check_database()" method

#############################################


logging.basicConfig(level=logging.INFO)


class DatabaseCreator:
    #  columns name
    client_server_database = "client_server"
    users_table = "users"
    username_column = "username"
    password_column = "password"
    join_date_column = "join_date"
    admin_column = "admin"
    messages_table = "messages"
    send_datetime_column = "send_datetime"
    sender_column = "sender"
    recipient_column = "recipient"
    message_column = "message"

    def __init__(self):
        load_dotenv()
        self.host = os.getenv("DB_HOST")
        self.port = os.getenv("DB_PORT")
        self.user = os.getenv("DB_USER")
        self.password = os.getenv("DB_PASSWORD")
        self.database = self.client_server_database
        self.expected_tables = {self.users_table, self.messages_table}
        self.expected_users_columns = {"id", self.username_column, self.password_column, self.join_date_column, self.admin_column}
        self.expected_messages_columns = {"id", self.send_datetime_column, self.sender_column, 
                                          self.recipient_column, self.message_column}

    def _connect_to_server(self):
        logging.info("Connecting to server")
        self.conn = psycopg2.connect(host=self.host, port=self.port, user=self.user, password=self.password, database=self.database)
        self.cur = self.conn.cursor()

    def _connect_to_database(self):
        logging.info("Connecting to database")
        self.conn = psycopg2.connect(host=self.host, port=self.port, user=self.user, password=self.password, database=self.database)
        self.cur = self.conn.cursor()

    def _close(self):
        logging.info("Closing connection")
        self.cur.close()
        self.conn.close()

    def _remake_tables(self):
        logging.info("Recreating tables")
        self._remove_tables()
        self._create_tables()

    def _remove_tables(self):
        logging.info("Dropping tables")
        self.cur.execute("DROP SCHEMA public CASCADE;")
        self.cur.execute("CREATE SCHEMA public;")
        self.conn.commit()

    def _create_tables(self):
        logging.info("Creating tables")
        query = """CREATE TABLE %s (
            id SERIAL PRIMARY KEY,
            %s VARCHAR(50) UNIQUE NOT NULL,
            %s VARCHAR(50),
            %s TIMESTAMP DEFAULT NOW(),
            %s BOOLEAN DEFAULT false
            );

        CREATE TABLE %s (
            id SERIAL PRIMARY KEY,
            %s TIMESTAMP DEFAULT NOW(),
            %s VARCHAR(50) NOT NULL,
            %s VARCHAR(50) NOT NULL,
            %s TEXT NOT NULL
            );"""
        self.cur.execute(query, (asis(self.users_table), asis(self.username_column),
                                asis(self.password_column), asis(self.join_date_column), asis(self.admin_column), 
                                asis(self.messages_table), asis(self.send_datetime_column), asis(self.sender_column), 
                                asis(self.recipient_column), asis(self.message_column)))
        self.conn.commit()

    def _create_database_if_not_exists(self):
        logging.info("Creating database if not exists")
        self.conn.autocommit = True
        query = "CREATE DATABASE %s ;"
        try:
            self.cur.execute(query, (asis(self.database),))
        except psycopg2.errors.DuplicateDatabase:
            pass

    def _check_tables(self):
        logging.info("Checking tables")
        current_tables = set()
        query = "SELECT table_name FROM information_schema.tables WHERE table_schema='public' AND table_type='BASE TABLE';"
        self.cur.execute(query)

        for table in self.cur.fetchall():
            current_tables.add(table[0])
        if current_tables != self.expected_tables:
            logging.error("Tables do not match")
            return False
        else:
            return True
        
    def _check_users_columns(self):
        logging.info("Checking users columns")
        current_columns = set()
        query = "SELECT column_name FROM information_schema.columns WHERE table_name = %s;"
        self.cur.execute(query, (self.users_table,))
        for column in self.cur.fetchall():
            current_columns.add(column[0])
        if current_columns != self.expected_users_columns:
            logging.error("Users columns do not match")
            return False
        else:
            return True

    def _check_messages_columns(self):
        logging.info("Checking messages columns")
        current_columns = set()
        query = "SELECT column_name FROM information_schema.columns WHERE table_name = %s;"
        self.cur.execute(query, (self.messages_table,))

        for column in self.cur.fetchall():
            current_columns.add(column[0])

        if current_columns != self.expected_messages_columns:
            logging.error("Messages columns do not match")
            return False
        else:
            return True

    def get_proper_database(self):
        logging.info("Getting proper database")
        self._connect_to_server()
        self._create_database_if_not_exists()
        self._connect_to_database()
        if not all([self._check_tables(), self._check_users_columns(), self._check_messages_columns()]):
            logging.info("Recreating tables")
            self._remake_tables()
        self._close()

