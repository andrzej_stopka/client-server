import os
import psycopg2
from psycopg2.extras import NamedTupleCursor
from dotenv import load_dotenv
import logging

logging.basicConfig(level=logging.INFO)

class DatabaseRunner:
    def __init__(self):
        load_dotenv()
        self.database = "client_server"
        self.host = os.getenv("DB_HOST")
        self.port = os.getenv("DB_PORT")
        self.user = os.getenv("DB_USER")
        self.password = os.getenv("DB_PASSWORD")
        self.conn = None
        self.cur = None

    def connect(self):
        if self.conn is None:
            logging.info("Connecting to database")
            self.conn = psycopg2.connect(
                database=self.database,
                user=self.user, 
                password=self.password, 
                host=self.host, 
                port=self.port
                )
            self.conn.autocommit = True
            self.cur = self.conn.cursor() 
            logging.info("Connected to database")
        return self.conn

    def get_column(self, query):
        logging.info("Executing query: %s", query)
        self.conn = self.connect()
        try:
            self.cur.execute(query)
            result = self.cur.fetchall()
            logging.info("Query executed successfully")
            return result
        except psycopg2.Error as e:
            logging.error("Error executing query: %s", e)
            return {"success": False, "message": "Internal error occurred, please try again later."}
        
    def get_all(self, query, values):
        logging.info("Executing query: %s with values: %s", query, values)
        self.conn = self.connect()
        self.cur = self.conn.cursor(cursor_factory=NamedTupleCursor)
        try:
            self.cur.execute(query, (values))
            result = self.cur.fetchall()
            logging.info("Query executed successfully")
            return result
        except psycopg2.Error as e:
            logging.error("Error executing query: %s", e)
            return {"success": False, "message": "Internal error occurred, please try again later."}  
        
    def get_cell(self, query, values):
        logging.info("Executing query: %s with values: %s", query, values)
        self.conn = self.connect()
        self.cur.execute(query, (values))
        try:
            cell_value = self.cur.fetchone()[0]
            logging.info("Query executed successfully")
            return cell_value
        except TypeError:
            logging.error("TypeError occurred")
            return False
        except psycopg2.Error as e:
            logging.error("Error executing query: %s", e)
            return {"success": False, "message": "Internal error occurred, please try again later."}
        
    def run_query(self, query, values):
        logging.info("Executing query: %s with values: %s", query, values)
        self.conn = self.connect()
        try:
            self.cur.execute(query, (values))
            self.conn.commit()
            logging.info("Query executed successfully")
            return {"success": True, "message": "Success."}
        except psycopg2.errors.UniqueViolation:
            self.conn.rollback()
            logging.error("UniqueViolation error occurred")
            return {"success": False, "message": "Entered username already exists in the database."}
        except psycopg2.Error as e:
            self.conn.rollback()
            logging.error("Error executing query: %s", e)
            return {"success": False, "message": "Internal error occurred, please try again later."}
    
    def disconnect(self):
        if self.conn:
            logging.info("Disconnecting from database")
            self.conn.close()
            logging.info("Disconnected from database")