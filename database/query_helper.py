class QueryHelper:
    @staticmethod
    def create_account(username, password):
        query = "INSERT INTO users (username, password) VALUES (%(username)s, %(password)s);"
        params = {"username": username, "password": password}
        return query, params

    @staticmethod
    def log_in(username):
        query = "SELECT password FROM users WHERE username = %(username)s;"
        params = {"username": username}
        return query, params

    @staticmethod
    def recipient_exists(username):
        query = "SELECT username FROM users WHERE username = %(username)s;"
        params = {"username": username}
        return query, params

    @staticmethod
    def send_message(sender, recipient, message):
        query = "INSERT INTO messages (sender, recipient, message) VALUES (%(sender)s, %(recipient)s, %(message)s);"
        params = {"sender": sender, "recipient": recipient, "message": message}
        return query, params

    @staticmethod
    def read_messages(username):
        query = "SELECT send_datetime, sender, message FROM messages WHERE recipient = %(username)s;"
        params = {"username": username}
        return query, params

    @staticmethod
    def clear_inbox(username):
        query = "DELETE FROM messages WHERE recipient = %(username)s;"
        params = {"username": username}
        return query, params

    @staticmethod
    def check_admin(username):
        query = "SELECT admin FROM users WHERE username = %(username)s;"
        params = {"username": username}
        return query, params

    @staticmethod
    def become_admin(username):
        query = "UPDATE users SET admin = true WHERE username = %(username)s;"
        params = {"username": username}
        return query, params

    @staticmethod
    def reset_user_password(username):
        query = "UPDATE users SET password = 'reset' WHERE username = %(username)s;"
        params = {"username": username}
        return query, params

    @staticmethod
    def delete_reset_requests(message):
        query = "DELETE FROM messages WHERE message = %(message)s;"
        params = {"message": message}
        return query, params

    @staticmethod
    def get_all_users():
        query = "SELECT username from users;"
        return query

    @staticmethod
    def delete_inbox(username):
        query = "DELETE FROM messages WHERE recipient = %(username)s;"
        params = {"username": username}
        return query, params

    @staticmethod
    def delete_user(username):
        query = "DELETE FROM users WHERE username = %(username)s;"
        params = {"username": username}
        return query, params