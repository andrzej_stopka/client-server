import logging
import socket
import datetime
import os
from dotenv import load_dotenv
from server_utils.communication import send_json, recv_json
from server_utils.server_utils import ServerSocket, ServerDatabase, ServerCommands

logging.basicConfig(level=logging.INFO)

class Server:
    def __init__(self):
        self.server_socket = ServerSocket()
        self.server_database = ServerDatabase()
        self.server_commands = ServerCommands(self.server_database)

    def start(self):
        conn = self.server_socket.start()
        welcome_message = self.server_commands.server_response("Welcome to the server, type 'help' to check all commands")
        send_json(conn, welcome_message)

        while True:
            data = recv_json(conn)
            command = data.get("command")
            username = data.get("username")
            password = data.get("password")
            recipient = data.get("recipient")
            message = data.get("message")

            logging.info(f"Received command: {command}, username: {username}, password: {password}, recipient: {recipient}, message: {message}")

            if command == "create" and not self.server_commands.user:
                response = self.server_commands.create_account(username, password)
                logging.info(f"Creating account for {username} with password {password}")
            elif command == "login" and not self.server_commands.user:
                response = self.server_commands.log_in(username, password)
                logging.info(f"Logging in user {username} with password {password}")
            elif command == "send" and self.server_commands.user:
                response = self.server_commands.send_message(self.server_commands.user, recipient, message)
                logging.info(f"Sending message from {self.server_commands.user} to {recipient} with message {message}")
            elif command == "read" and self.server_commands.user:
                response = self.server_commands.read_messages(self.server_commands.user)
                logging.info(f"Reading messages for user {self.server_commands.user}")
            elif command == "clear" and self.server_commands.user:
                response = self.server_commands.clear_inbox(self.server_commands.user)
                logging.info(f"Clearing inbox for user {self.server_commands.user}")
            elif command == "admin" and self.server_commands.admin:
                admin_command = data["admin_command"]
                match admin_command:
                    case "reset":
                        response = self.server_commands.reset_user_password(username)
                        logging.info(f"Resetting password for user {username}")
                    case "sendall":
                        response = self.server_commands.send_all(message)
                        logging.info(f"Sending message to all users with message {message}")
                    case "readfor":
                        response = self.server_commands.read_messages(username)
                        logging.info(f"Reading messages for user {username}")
                    case "delete":
                        response = self.server_commands.delete_user(username)
                        logging.info(f"Deleting user {username}")
            elif command == "becomeadmin":
                response = self.server_commands.become_admin(username)
                logging.info(f"Making user {username} an admin")
            elif command == "off":
                response = self.server_commands.log_off()
                logging.info(f"Logging off user {self.server_commands.user}")
            elif command == "uptime":
                response = self.server_commands.uptime()
                logging.info(f"Getting server uptime")
            elif command == "info":
                response = self.server_commands.info()
                logging.info(f"Getting server info")
            elif command == "help":
                response = self.server_commands.help()
                logging.info(f"Getting help")
            elif command == "stop":
                logging.info("Stopping server")
                break
            else:
                response = self.server_commands.server_response("Command not recognized, type 'help' to check all commands or try again.")
                logging.info(f"Unknown command {command}")

            send_json(conn, response)

if __name__ == "__main__":
    logging.info("Starting server")
    server = Server()
    server.start()