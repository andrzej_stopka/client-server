import json


def send_json(client_socket, data):
    json_data = json.dumps(data)
    client_socket.send(json_data.encode("utf-8"))


def recv_json(client_socket):
    data = client_socket.recv(1024).decode("utf-8")
    data = json.loads(data)
    return data
