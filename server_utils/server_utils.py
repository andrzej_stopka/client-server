import logging
import socket
import datetime
import os
from dotenv import load_dotenv
from database.db_creator import DatabaseCreator
from database.db_runner import DatabaseRunner
from database.query_helper import QueryHelper

logging.basicConfig(level=logging.INFO)

class ServerSocket:
    def __init__(self):
        load_dotenv()
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.host = os.getenv("HOST")
        self.port = int(os.getenv("PORT"))

    def start(self):
        self.socket.bind((self.host, self.port))
        self.socket.listen(1)
        logging.info(f"Server is listening on {self.host}:{self.port}")
        conn, addr = self.socket.accept()
        logging.info(f"Connected by {addr}")
        return conn

class ServerDatabase:
    def __init__(self):
        self.db_creator = DatabaseCreator()
        self.db_creator.get_proper_database()
        self.db_runner = DatabaseRunner()

class ServerCommands:
    def __init__(self, server_database):
        self.db_runner = server_database.db_runner
        self.start_time = datetime.datetime.now()
        self.user = None
        self.admin = False

    def server_response(self, message):
        return {"[ANDRZEJ STOPKA TCP/IP SERVER]": message}

    def uptime(self):
        return {"[UPTIME]": str(datetime.datetime.now() - self.start_time)}

    def info(self):
        return {
            "[TITLE]": "My First Client/Server Application",
            "[VERSION]": "3.1.0",
            "[DATE CREATED]": "11.04.2023",
        }

    def help(self):
        help = {
            "[CREATE YOUR ACCOUNT]": "- create",
            "[LOG IN YOUR ACCOUNT]": "- login",
            "[SHOW THE SERVER'S UPTIME]": "- uptime",
            "[SHOW THE SERVER'S INFO]": "- info",
            "[SHOW ALL AVAILABLE COMMANDS]": "- help",
            "[STOP THE SERVER]": "- stop"
        }
        if self.user:
            del help["[CREATE YOUR ACCOUNT]"]
            del help["[LOG IN YOUR ACCOUNT]"]
            help = {
                "[SEND A MESSAGE TO OTHER USER]": "- send",
                "[READ YOUR MESSAGES]": "- read",
                "[REMOVE ALL YOUR MESSAGES]": "- clear",
                "[LOG OUT OF YOUR ACCOUNT]": "- off",
                **help,
            }
        return help

    def create_account(self, username, password):
        query, params = QueryHelper.create_account(username, password)
        result = self.db_runner.run_query(query, params)
        if result["success"] is True:
            logging.info(f"Account created for {username}")
            return self.server_response(f"Your account '{username}' has been created")
        else:
            logging.error(f"Failed to create account for {username}: {result['message']}")
            return self.server_response(f"FAILED! {result['message']}")

    def check_user_admin(self, username):
        query, params = QueryHelper.check_admin(username)
        result = self.db_runner.get_cell(query, params)
        return result

    def become_admin(self, username):
        query, params = QueryHelper.become_admin(username)
        result = self.db_runner.run_query(query, params)
        if result["success"] is True:
            self.admin = True
            logging.info(f"User {username} is now an admin")
            return self.server_response(f"You have been given admin privileges")
        else:
            logging.error(f"Failed to make {username} an admin: {result['message']}")
            return self.server_response(f"FAILED! {result['message']}")

    def send_reset_request(self, username):
        users = self.get_all_users()
        message = f"{username} send a reset request"
        for user in users:
            admin = self.check_user_admin(user)
            if admin:
                self.send_message(username, user, message)

    def log_in(self, username, password):
        if password == "reset":
            self.send_reset_request(username)
            return self.server_response("Your reset has been sent, wait up to 12 hours and type your username as a password")
        query, params = QueryHelper.log_in(username)
        result = self.db_runner.get_cell(query, params)
        if result == password:
            self.user = username
            self.admin = self.check_user_admin(username)
            logging.info(f"User {username} logged in")
            return self.server_response("You have logged in successfully")
        else:
            logging.error(f"Failed to log in user {username}: wrong password")
            return self.server_response("Wrong login or password was given, try again")

    def get_messages(self, username):
        query, params = QueryHelper.read_messages(username)
        messages = self.db_runner.get_all(query, params)
        return messages

    def user_exists(self, username):
        query, params = QueryHelper.recipient_exists(username)
        user_exists = self.db_runner.get_cell(query, params)
        return user_exists

    def send_message(self, sender, recipient, message):
        recipient_exists = self.user_exists(recipient)
        if not recipient_exists:
            logging.error(f"User {recipient} does not exist")
            return self.server_response("There is no such a user")
        messages = self.get_messages(recipient)
        recipient_admin = self.check_user_admin(recipient)
        if len(messages) >= 5 and not recipient_admin:
            logging.error(f"User {recipient} has full inbox")
            return self.server_response(f"User {recipient} has full inbox.")
        query, params = QueryHelper.send_message(sender, recipient, message)
        result = self.db_runner.run_query(query, params)
        if result["success"] is True:
            logging.info(f"Message sent from {sender} to {recipient}")
            return self.server_response(f"Your message to '{recipient}' has been sent")
        else:
            logging.error(f"Failed to send message from {sender} to {recipient}: {result['message']}")
            return self.server_response(f"FAILED! {result['message']}")

    def read_messages(self, user):
        messages = self.get_messages(user)
        if len(messages) == 0:
            logging.info(f"User {user} has no messages")
            return self.server_response("Your inbox is empty")
        result = dict()
        for i, row in enumerate(messages):
            row_dict = f"TIME: {row[0].strftime('%Y-%m-%d %H:%M:%S')}\nSENDER: {row[1]}\nMESSAGE: {row[2]}"
            result[f"[Message {i + 1}]"] = row_dict
        if len(messages) == 5 and not self.admin:
            result["[ANDRZEJ STOPKA TCP/IP SERVER]: Your inbox is full, clear your inbox otherwise you can't receive more messages"]
        return result
    
    def clear_inbox(self, username):
        query, params = QueryHelper.clear_inbox(username)
        result = self.db_runner.run_query(query, params)
        if result["success"] is True:
            logging.info(f"User {username} inbox cleared")
            return self.server_response("Your inbox has been cleared")
        else:
            logging.error(f"Failed to clear inbox for user {username}: {result['message']}")
            return self.server_response(f"FAILED! {result['message']}")

    def reset_user_password(self, username):
        query, params = QueryHelper.reset_user_password(username)
        result = self.db_runner.run_query(query, params)
        if result["success"] is True:
            self.delete_reset_requests(username)
            logging.info(f"User {username} password has been reset")
            return self.server_response(f"{username} password has been reset")
        else:
            logging.error(f"Failed to reset password for user {username}: {result['message']}")
            return self.server_response(f"FAILED! {result['message']}")

    def delete_reset_requests(self, username):
        message = (f"{username} send a reset request",)
        query, params = QueryHelper.delete_reset_requests(message)
        self.db_runner.run_query(query, params)

    def get_all_users(self):
        query = QueryHelper.get_all_users()
        result = self.db_runner.get_column(query)
        result = [tup[0] for tup in result]
        return result

    def send_all(self, message):
        users = self.get_all_users()
        for user in users:
            self.send_message(user, user, message)
        logging.info(f"Message sent to all users")
        return self.server_response(f"You sent your message to all users")

    def delete_user_inbox(self, username):
        query, params = QueryHelper.delete_inbox(username)
        inbox_result = self.db_runner.run_query(query, params)
        return inbox_result

    def delete_user(self, username):
        inbox_result = self.delete_user_inbox(username)
        query, params = QueryHelper.delete_user(username)
        user_result = self.db_runner.run_query(query, params)
        if inbox_result["success"] is True and user_result["success"] is True:
            logging.info(f"User {username} deleted")
            return self.server_response(f"{username} account has been deleted")
        else:
            logging.error(f"Failed to delete user {username}: {user_result['message']}")
            return self.server_response(f"FAILED! {user_result['message']}")

    def log_off(self):
        self.user = None
        logging.info(f"User logged off")
        return self.server_response("You have been logged out")