import unittest

from server import Server


class TestServer(unittest.TestCase):
    server = Server()
    username = "testusername"
    password = "testpassword"
    
    def test_server_response(self):
        test_message = "test message"
        response = self.server.server_response(test_message)

        self.assertEqual(response, {"[ANDRZEJ STOPKA TCP/IP SERVER]": test_message})
    
    def test_create_account(self):
        self.server.delete_user(self.username)
        user_exist_before = self.server.user_exists(self.username)
        self.assertFalse(user_exist_before)
        self.server.create_account(self.username, self.password)
        user_exists_after = self.server.user_exists(self.username)
        self.assertTrue(user_exists_after)
    
    def test_admin(self):
        """
        Test for check_user admin and become_admin
        """
        admin_before = self.server.check_user_admin(self.username)
        self.assertFalse(admin_before)
        self.server.become_admin(self.username)
        admin_after = self.server.check_user_admin(self.username)
        self.assertTrue(admin_after)

    def test_log_in(self):
        wrong_password = "wrong_password"
        result = self.server.log_in(self.username, wrong_password)
        _, message = result.popitem()
        self.assertEqual(message, "Wrong login or password was given, try again")
        logged_result = self.server.log_in(self.username, self.password)
        _, message = logged_result.popitem()
        self.assertEqual(message, "You have logged in successfully")

    def test_user_exists(self):
        non_existent_user = "no exist"
        exists = self.server.user_exists(non_existent_user)
        self.assertFalse(exists)
        exists = self.server.user_exists(self.username)
        self.assertTrue(exists)

    def test_send_message(self):
        non_existent_recipient = "no exist"
        response = self.server.send_message(self.username, non_existent_recipient, "test")
        _, response_message = response.popitem()
        self.assertEqual(response_message, "There is no such a user")
        response = self.server.send_message(self.username, self.username, "test")
        _, response_message = response.popitem()
        self.assertEqual(response_message, f"Your message to '{self.username}' has been sent")

    def test_clear_inbox(self):
        inbox_length = len(self.server.get_messages(self.username))
        lenght_more_than_zero = inbox_length > 0
        self.assertTrue(lenght_more_than_zero)
        self.server.clear_inbox(self.username)
        inbox_length = len(self.server.get_messages(self.username))
        lenght_equals_zero = inbox_length == 0
        self.assertTrue(lenght_equals_zero)

    def test_delete_user(self):
        user_to_delete = "delete user"
        self.server.create_account(user_to_delete, "password")
        user_exists = self.server.user_exists(user_to_delete)
        self.assertTrue(user_exists)
        self.server.delete_user(user_to_delete)
        user_exists = self.server.user_exists(user_to_delete)
        self.assertFalse(user_exists)

if __name__ == "__main__":
    unittest.main()